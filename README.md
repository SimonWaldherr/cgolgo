#cgolGo

[![Travis CI](https://img.shields.io/travis/SimonWaldherr/cgolGo.svg?style=flat)](https://travis-ci.org/SimonWaldherr/cgolGo)
[![Flattr donate button](https://img.shields.io/badge/donate%20via-flattr-green.svg)](https://flattr.com/submit/auto?user_id=SimonWaldherr&url=http%3A%2F%2Fgithub.com%2FSimonWaldherr%2FcgolGo "Donate monthly to this project using Flattr") 
[![PayPal donate button](https://img.shields.io/badge/donate%20via-paypal-blue.svg)](https://www.paypal.me/SimonWaldherr "Donate to this project via PayPal.me") 
[![License MIT](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](https://raw.githubusercontent.com/SimonWaldherr/cgolGo/master/LICENSE)  

## conway's game of life in golang

[Conway's Game of Life](http://en.wikipedia.org/wiki/Conway's_Game_of_Life)  
in [golang](http://en.wikipedia.org/wiki/Go_(programming_language))  

## examples

01.gif

[![01.gif](http://simonwaldherr.github.io/cgolGo/output/01.gif)](https://github.com/SimonWaldherr/cgolGo/blob/master/structures/01.txt)  

02.gif

[![02.gif](http://simonwaldherr.github.io/cgolGo/output/02.gif)](https://github.com/SimonWaldherr/cgolGo/blob/master/structures/02.txt)  

03.gif

[![03.gif](http://simonwaldherr.github.io/cgolGo/output/03.gif)](https://github.com/SimonWaldherr/cgolGo/blob/master/structures/03.txt)  

15.gif

[![15.gif](http://simonwaldherr.github.io/cgolGo/output/15.gif)](https://github.com/SimonWaldherr/cgolGo/blob/master/structures/15.txt)  

## license

[MIT](https://github.com/SimonWaldherr/cgolGo/blob/master/LICENSE)  
